#!/usr/bin/env python


class APP_SEL():
    DEFAULT = 0
    TOLL = 1
    CUSTOM = 2
    CUSTOM1 = 3
    CUSTOM2 = 4
    CUSTOM3 = 5
    CUSTOM4 = 6

class DEVICE_CONFIGURATION():
    fCallBackMain =  True
    fLoopOpearation = True
    uchDeviceOpMode = 0
    uchMaxTagVicinityCount = 5
    uchTagRepeateCount = 3
    uiSensorOffTime = 200
    uiSensorOnTime = 200

class SETTING_INVENTORY():
    fEnableFastID = False
    fEnableTagFocus = False
    fEnableTagOperation = True
    fReqCRC = False
    fReqPC = False
    fReqRSSI = True
    fReqTID = True
    fReqTimestamp = True
    fReqAntenna = True
    uchRfMode = 0
    uiPopulationTag = 16
    uisearchMode = 0
    uiSession = 1
    uiStopDuration = 0
    uiStopTagCount = 0

class DEVICE_MODE():
    devmod = 1
