#!/usr/bin/env python

import threading
import Queue
import os
import time
import datetime
import configparser

"""import application modules"""
from rs2000_wrapper import UhfLib
#from uhf_db_handler import UHFDBHandler
from storage import Storage
#from apistorage import Apistorage
from addonapi import DEVICE_CONFIGURATION


"""get tag details"""
def callback_uhf(row_number):

    if row_number < 25 :

        tagval = rfid_obj.get_tag_data(row_number)
        epcid = tagid = usrid = tag_rssi = ''

        """get epcid"""
        if(tagval.contents.epc_len != 0):
            ilen = tagval.contents.epc_len
            for i,b in enumerate(tagval.contents.epc):
                if(ilen > 0):
                    epcid = epcid + ('%02X' % b)
                    ilen = ilen -1

        """uchDevicemode 0,1,2,3"""
        if(dev_cfg_obj.uchDeviceOpMode == 0 or dev_cfg_obj.uchDeviceOpMode == 3 or dev_cfg_obj.uchDeviceOpMode == 5):

            """get usrid"""
            if(tagval.contents.data_len != 0):
                ilen = tagval.contents.data_len
                for i,b in enumerate(tagval.contents.usr_data):
                    if(ilen > 0):
                        usrid = usrid + ('%02X' % b)
                    ilen = ilen - 1
        else:
            """get tagid"""
            if(tagval.contents.tid_len != 0):
                ilen = tagval.contents.tid_len
                for i,b in enumerate(tagval.contents.tid):
                    if(ilen > 0):
                        tagid =  tagid + ('%02X' % b)
                        ilen = ilen - 1

        """get tag_rssi"""
        if(tagval.contents.has_rssi):
            tag_rssi = str(tagval.contents.rssi)


        """add time-stamp"""
        time_stamp = datetime.datetime.now()
        timestamp = time_stamp.strftime("%d-%m-%Y %H:%M:%S %p")

        """put data in Q"""
        __slots__=(epcid,tagid,timestamp,reader_id,usrid,tag_rssi,0)
        print __slots__
        buzzq.put(1)
        dataq.put(__slots__)
        savetodb(dataq)
        return 0
    else:
        print "no tag details found"

"""save dataQ data to database"""
def savetodb(recvdataq):
    __slots__=[]
    if recvdataq.empty() == True:
        print "DataQ is Empty"
    else:
        datam = recvdataq.get()
        __slots__.append(datam)
        storage.put_qdata(__slots__)
        #apistorage.put_qdata(__slots__)
        __slots__ = []

"""Trigger Buzzer"""
def buzzer():
    while (1):
        if buzzq.empty() == False:
            bzzget = buzzq.get()
            os.system("echo 1 > /sys/class/gpio/gpio122/value")
            time.sleep(0.1)
            os.system("echo 0 > /sys/class/gpio/gpio122/value")
    
"""uhf worker thread"""
def start_uhf_worker():
    while(1):

        """set comp port"""
        error = rfid_obj.uhf_startup('/dev/ttymxc4')

        if error == -3:
            print "No valid License found"
            rfid_obj.uhf_cleanup()
            return -1
        
        """set antenna level and power level"""
        rfid_obj.uhf_antenna(power_level,antennal)

        """call callback function"""
        rfid_obj.uhf_set_callback(callback_uhf)
        
        trf_app_error = rfid_obj.trf_app(customode)
        if trf_app_error:
            print "Error in initialising trf_app"
            rfid_obj.uhf_cleanup()
            return -1
       
        uhf_read_error = rfid_obj.uhf_read(2,6,0)
        if uhf_read_error:
            print "Error in initiating uhf_read"
            rfid_obj.uhf_cleanup()
            return -1

        rfid_obj.start_inventory()

        error_trf_app_handler = rfid_obj.trf_app_handler(customode)
        if error_trf_app_handler:
            print "Error in trf_app_handler"
            rfid_obj.uhf_cleanup()
            return -1
       
        return 0
 
def main():

    """start thread uhf worker"""
    t1 = threading.Thread(target=start_uhf_worker)
    t2 = threading.Thread(target=buzzer)
    t1.start()
    t2.start()
    t2.join()

if __name__ == '__main__':

    """set GPIO"""
    os.system("echo 110 > /sys/class/gpio/export")
    os.system("echo 'out' > /sys/class/gpio/gpio110/direction")
    os.system("echo 1 > /sys/class/gpio/gpio110/value")
    os.system("echo 117 > /sys/class/gpio/export")
    os.system("echo 'out' > /sys/class/gpio/gpio117/direction")
    os.system("echo 0 > /sys/class/gpio/gpio117/value")
    os.system("echo 122 > /sys/class/gpio/export")             
    os.system("echo 'out' > /sys/class/gpio/gpio122/direction")
    os.system("echo 0 > /sys/class/gpio/gpio122/value")


    """create module objects
    : UhfLib
    : Queue
    : APP_SEL
    : DEVICE_CONFIGURATION
    : Configuration ini
    """

    rfid_obj = UhfLib()
    dataq = Queue.Queue(maxsize=0)
    buzzq = Queue.Queue(maxsize=0)
    dev_cfg_obj = DEVICE_CONFIGURATION()
    config = configparser.ConfigParser(strict=False)

    """get readerid,power_level,antenna level and custom mode"""
    config.read('/etc/uhf/reader.ini')

    #custom mode
    customode = int(config['Reader']['custom'])
    
    #readerid
    reader_id = str(config['Reader']['readerid'])

    #power level
    power_level = int(config['Reader']['powerdb'])

    #antenna level
    antennal=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    ant1=config['Reader']['antenna1']
    ant2=config['Reader']['antenna2']
    ant3=config['Reader']['antenna3']
    ant4=config['Reader']['antenna4']

    if ant1 == "True":
        antennal[0] = 1

    if ant2 == "True":
        antennal[1] = 2

    if ant3 == "True":
        antennal[2] = 3

    if ant4 == "True":
        antennal[3] = 4

    """get storage class"""
    storage = Storage()
    #apistorage = Apistorage()

    """start main"""
    main()
