import sqlite3

class UHFDBHandler:
    def __init__(self):
        self.DB_PATH = '/usr/local/db/database.db'
        self.creat_db()

    def getdbcon(self):
        try:
            conn = sqlite3.connect(self.DB_PATH)
        except Exception, e:
            print "error in db conn", e
        return conn

    def get_config_prm(self, skey):
        con = self.getdbcon()
        sql = str("select * from configstorage where cfg_nm = '"+skey+"'")
        cur = con.execute(sql)
        data = cur.fetchall()
        sval = ''
        for row in data:
            sval = str(row[1])
        cur.close()
        con.close()
        return sval

    def config_db(self):
        CARD_REPEAT = float(str(self.get_config_prm('card_repeat_time')))
        SENDING = str(self.get_config_prm('sending'))
        READER_ID = str(self.get_config_prm('readerId'))
        POWER = int(str(self.get_config_prm('antenna_power')), 10)
        DB_PATH = str(self.get_config_prm('db_path'))
        DEVICE_ID = str(self.get_config_prm('device_id'))
        crcval = str(self.get_config_prm('crc'))
        servgpsintv = int(str(self.get_config_prm('servgpsintv')),10)
        glatc = float(str(self.get_config_prm('latcorr')))
        glonc =  float(self.get_config_prm('loncorr'))
        currentindex = -1
        powerLevel1 = int(str(self.get_config_prm('antenna_power')), 10)
        sel_antenna = str(self.get_config_prm('sel_antenna'))
        http_timeout =  str(self.get_config_prm('http_timeout'))
        api_url =  str(self.get_config_prm('api_url'))
        deb_msg = int(str(self.get_config_prm('deb_msg')), 10)
        D = False
        if deb_msg == 1:
            D = True
        jdict = { 'CARD_REPEAT':CARD_REPEAT,'SENDING':SENDING ,'READER_ID':READER_ID ,'POWER':POWER,
                  'DB_PATH':DB_PATH,'DEVICE_ID':DEVICE_ID,'crcval':crcval,'servgpsintv':servgpsintv,'glatc':glatc,
                  'glonc':glonc,'currentindex':currentindex,'powerLevel1': powerLevel1, 'sel_antenna':sel_antenna,
                  'http_timeout':http_timeout,'api_url':api_url,'deb_msg':deb_msg, 'D':D }
        return jdict

    def parse_nw_prm(self,skey,skey1):
        lstprm1 = []
        lstprm2 = []
        sval = ''
        lstprm1 = skey.split(',')
        for val in lstprm1:
            lstprm1 = val.split(':')
            if lstprm1[0] == skey1:
                sval = lstprm1[1]
        return sval

    def nw_prms(self):
        nwwifi = str(self.get_config_prm('wifi'))
        wifissid = self.parse_nw_prm(nwwifi, 'ssid')
        wifipswd = self.parse_nw_prm(nwwifi, 'pswd')
        wifidhcp = self.parse_nw_prm(nwwifi, 'dhcp')
        wifiip =   self.parse_nw_prm(nwwifi, 'ip')
        wifigway = self.parse_nw_prm(nwwifi, 'gateway')
        wifidns1 = self.parse_nw_prm(nwwifi, 'dns1')
        wifidns2 = self.parse_nw_prm(nwwifi, 'dns2')
        wifisubnet = self.parse_nw_prm(nwwifi, 'subnet')
        nweth = str(self.get_config_prm('ethernet'))
        ethdhcp = self.parse_nw_prm(nweth, 'dhcp')
        ethip = self.parse_nw_prm(nweth, 'ip')
        ethgway = self.parse_nw_prm(nweth, 'gateway')
        ethdns1 = self.parse_nw_prm(nweth, 'dns1')
        ethdns2 = self.parse_nw_prm(nweth, 'dns2')
        ethsubnet = self.parse_nw_prm(nweth, 'subnet')
        nwgprs = str(self.get_config_prm('gprs'))
        gprsunm = self.parse_nw_prm(nwgprs, 'unm')
        gprspwd = self.parse_nw_prm(nwgprs, 'pwd')
        gprsapn = self.parse_nw_prm(nwgprs, 'apn')
        jdict = { 'nwwifi':nwwifi,'nweth':nweth ,'nwgprs':nwgprs ,'wifissid':wifissid,
                  'wifipswd':wifipswd,'wifidhcp':wifidhcp,'wifiip':wifiip ,'wifigway':wifigway,
                  'wifidns1':wifidns1,'wifidns2':wifidns2,'wifisubnet':wifisubnet,'ethdhcp':ethdhcp,
                  'ethip':ethip,'ethgway':ethgway,'ethdns1':ethdns1,'ethdns2':ethdns2,
                  'ethsubnet':ethsubnet, 'gprsunm':gprsunm, 'gprspwd':gprspwd, 'gprsapn':gprsapn }
        return jdict

    def upd_apiconfig_prm(self, skey, sval):
        con = self.getdbcon()
        cur = con.cursor()
        sql = str("update configstorage set cfg_val = '"+sval+"' where cfg_nm = '" + skey + "'")
        cur = con.execute(sql)
        con.commit()
        cur.close()
        con.close()
        return sval

    def gettrflaccess(self):
        rdict = {}
        try:
            conn = self.getdbcon()
            cur = conn.execute("select * from trflaccess")
            for row1 in cur.fetchall():
                rdict = {
                'enaether': row1[0],
                'enawifi': row1[1],
                'enagprs': row1[2],
                'enagps': row1[3],
                'enaserv': row1[4],  
                'enasock': row1[5],
                'enagui': row1[6],
                'enalog': row1[7]                              
                }
            cur.close()
            conn.close()
        except Exception, e:
            print e
        return rdict

    def setTRFLAccess(self):
        try:
            con = self.getdbcon()
            cur = con.cursor()
            cur.execute("SELECT * FROM trflaccess")
            temp_acs = cur.fetchone()
            cur.close()
            if temp_acs:
                pass
            else:
                cur1 = con.cursor()
                cur1.execute("INSERT INTO trflaccess(enaether,enawifi,enagprs,enagps,enaserv,enasock,enagui,enalog) VALUES('1','0','0','0','0','0','0','0')")
                con.commit()
                cur1.close()
            con.close()
        except Exception, e:
            print 'exception in db',e

    def creat_db(self): #"create db if not exists"
        try:
            con = self.getdbcon()
            cur = con.cursor()
            cur.execute("CREATE TABLE IF NOT EXISTS customstorage(\
            epc_id TEXT NOT NULL, tag_id TEXT NOT NULL,\
            timestamp TEXT NOT NULL,reader_id TEXT NOT NULL,usr_id TEXT,tag_rssi TEXT,flag integer)")
            cur.execute("CREATE TABLE IF NOT EXISTS trflaccess(enaether TEXT, enawifi TEXT,\
            enagprs TEXT, enagps TEXT, enaserv TEXT, enasock TEXT, enagui TEXT, enalog TEXT)")
            cur.execute("CREATE TABLE IF NOT EXISTS configstorage(cfg_nm TEXT,cfg_val TEXT,cfg_desc TEXT)")
            cur1 = con.execute("SELECT count(*) FROM configstorage")
            data = cur1.fetchone()
            if data[0] == 2:
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('http_timeout','2','timeout inseconds incase remote server not avialable')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('card_repeat_time','10','Interval during which same card is no repeated')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('api_url','http://trackrf123.pythonanywhere.com/uhfgetMInfo'\
                ,'remote server url')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('antenna_power','314','antenna power')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('drly','1','Delay for relay')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('db_path','database.db','sqlite3 database path')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('sel_antenna','1','antenna number')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('deb_msg','0','Enable/disable debug prints')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('glatc','0','gpscorr')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('glonc','0','gps long corr')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('crc','1234','crc code sent to remote server')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('latcorr','0','Gps latitude correction factor')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('loncorr','0','Gps longitude correction factor')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('servurlgps','http://trackrf123.pythonanywhere.com/uhfgetGps'\
	            ,'remote server url for gps data')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('servurlpool','http://trackrf123.pythonanywhere.com/uhfgetMInfo'\
	            ,'remote server url for pool data')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('servgpsintv','5','Interval to send Gps data to server')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('ethernet','dhcp:0,ip:192.168.0.210,gateway:192.168.0.1\
,dns1:8.8.8.8,dns2:8.8.4.4,subnet:255.255.255.0','parameters to set IP using ethernet')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('wifi','ssid:TRFL,pswd:trfl@123,dhcp:0,ip:192.168.0.10\
,gateway:192.168.0.1,dns1:8.8.8.8,dns2:8.8.4.4,subnet:255.255.255.0','parameters to set IP using wifi')")
                cur.execute("INSERT INTO configstorage(cfg_nm,cfg_val,cfg_desc) values('gprs','apn:aircelgprs,unm:guest,pwd:guest','parameters to set gprs network')")
            con.commit()
            cur.close()
            con.close()
            self.setTRFLAccess()
        except Exception, e:
            print 'exception in db',e
