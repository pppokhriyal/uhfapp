from ctypes import CDLL,c_uint,c_char_p,CFUNCTYPE,c_long,c_int,c_uint16,POINTER,c_uint8,Structure,c_ubyte,c_bool,c_longlong,byref,_pointer_type_cache
from enum import IntEnum
import gc

LIBRARY_PATH = '/lib/trfl_library.so'

class UhfLib(object):
    """Python wrapper for rs2000 C API's"""

    def __init__(self, library_path=LIBRARY_PATH):
        self.lib = CDLL(library_path)

    def run_safely(self, func, *args):
        """call c funtions with Exception handling

        :param func: function to run
        :type func: valid python function
        :return: if success return value from fun else -55
        """
        try:
            return func(*args)
        except:
            return -55

    def uhf_startup(self, com_port):
        """starts the communication to rs2000 through the given serial port

        :param com_port: system allocated serial port (COM3, /dev/ttyUSB0)
        :type com_port: string
        :return: C API return code
        :rtype: int
        """
        return self.lib.uhf_startup(c_char_p(com_port.encode('utf-8')))

    def uhf_cleanup(self):
        """stops the communication and resets the device

        :return: C API return code
        :rtype: int
        """
        return self.lib.uhf_cleanup()

    def uhf_inventory(self):
        """starts an inventory operation

        :return: C API return code
        :rtype: int
        """
        return self.lib.uhf_inventory()

    def uhf_read(self, mem_bank, word_count, word_pointer):
        """read data after inventory

        :param mem_bank: memory bank to read data from [tID-1, EPC-2, user-3]
        :type mem_bank: int
        :param word_count: number of words to read [1 word -> 2 ASCII char -> 4 hex]
        :type word_count: int
        :param word_pointer: offset value to start with
        :type word_pointer: int
        :return: status, C API return code
        :rtype: int
        """
        return self.lib.uhf_read(
            c_uint(mem_bank),
            c_uint(word_count),
            c_uint(word_pointer))

    def uhf_write(self, mem_bank, word_count, word_pointer, data):
        """writes to the card after inventory

        :param mem_bank: memory bank to write data [tID-1, EPC-2, user-3]
        :type mem_bank: int
        :param word_count: number of words to write [1 word -> 2 ASCII char -> 4 hex]
        :type word_count: int
        :param word_pointer: offset value to start with
        :type word_pointer: int
        :param data: list of words to write
        :type data: list
        :return: status, C API return code
        :rtype: int
        """
        n_type = c_uint16 * word_count
        data_to_write = n_type()
        for i in range(0, word_count):
            data_to_write[i] = data[i]
        return self.lib.uhf_write(
            c_uint(mem_bank),
            c_uint(word_count),
            c_uint(word_pointer),
            data_to_write)

    def uhf_write_epc(self, word_count, data):
        """writes to the epc after inventory

        :param word_count: number of words to write [1 word -> 2 ASCII char -> 4 hex]
        :type word_count: int
        :param data: list of words to write
        :type data: list
        :return: status, C API return code
        :rtype: int
        """
        n_type = c_uint16 * word_count
        data_to_write = n_type()
        for i in range(0, word_count):
            data_to_write[i] = data[i]
        return self.lib.uhf_write_epc(
            c_uint(word_count),
            data_to_write)

    def uhf_antenna(self, antenna_power, antenna_seq):
        """set antenna parameters

        :param antenna_power: output power in dB maximux 31.5dB
        :type antenna_power: int
        :param antenn_seq: logical antennas and their order
        :type antenna_seq: list
        :return: status, C API return code
        :rtype: int
        """
        antenna_seq_type = c_uint16 * 16
        antenna_s = antenna_seq_type()
        for i in range(0, 16):
            antenna_s[i] = antenna_seq[i]
        return self.lib.uhf_antenna(
            c_uint(antenna_power),
            antenna_s)

    def uhf_set_value(self, key, value):
        """set configuration keys

        :param key: key to set, refer to impinj ITK-C documentation
        :type key: int
        :param value: value for the given key, refer ITK-C documentation
        :type value: int
        :return: status, C API return code
        :rtype: int
        """
        return self.lib.uhf_set_value(
            c_uint(key),
            c_uint(value))

    def uhf_set(self, key, bank_index, value_index, value):
        """set configuration keys

        :param key: key to set, refer to impinj ITK-C documentation
        :type key: int
        :param bank_index: bank_index for an indexed key, refer ITK-C documentation
        :type bank_index: int
        :param value_index: value_index, refer ITK-C documentation
        :type value_index: int
        :param value: value for the given key, refer ITK-C documentation
        :type value: int
        :return: status, C API return code
        :rtype: int
        """
        return self.lib.uhf_set(
            c_uint(key),
            c_uint(bank_index),
            c_uint(value_index),
            c_uint(value))

    def uhf_set_callback(self, function):
        """set callback function

        :param function: valid python callback function with with compatible signature
        :type function: test_fun(cmd_type,cmd_status,epc_len,epc, tag_len, tags)
        :return: status, C API return code
        :rtype: int
        """
        allocator = CFUNCTYPE(c_long, c_int)
        self.lib.uhf_set_callback.argtypes = [allocator]
        return self.lib.uhf_set_callback(allocator(function))

    def trf_app(self, app_type):
        """ app selection api

        :param function: selection of app type (DEFAULT, TOLL, SELECTION)
        :return: status, C API return code
        :rtype: int
        """
        # Define the types we need.
        class CtypesEnum(IntEnum):
            """A ctypes-compatible IntEnum superclass."""
            @classmethod
            def from_param(cls, obj):
                return int(obj)
        class RFL_APP(CtypesEnum):
            DEFAULT = 0
            TOLL = 1
            SELECTION = 2
        self.lib.trf_app.argtypes = [RFL_APP]
        self.lib.trf_app.restype = c_int
        return self.lib.trf_app(app_type)

    def trf_app_handler(self, app_type):
        """ app selection handler api

        :param function: selection of app type (DEFAULT, TOLL, SELECTION)
        :return: status, C API return code
        :rtype: int
        """
        # Define the types we need.
        class CtypesEnum1(IntEnum):
            """A ctypes-compatible IntEnum superclass."""
            @classmethod
            def from_param(cls, obj):
                return int(obj)
        class RFL_APP1(CtypesEnum1):
            DEFAULT = 0
            TOLL = 1
            SELECTION = 2
        self.lib.trf_app_handler.argtypes = [RFL_APP1]
        self.lib.trf_app_handler.restype = c_int

        return self.lib.trf_app_handler(app_type)

    def get_tag_data(self, row_no):
        """ get tag data api

        :param function: select the row index of data
        :return: tag data class object, C API return code
        :rtype: class object
        """
        ID_MAX_DATA_SIZE = 20
        ARRAY5 = c_ubyte * ID_MAX_DATA_SIZE
        class TAG_DATA(Structure):
            _fields_ = [("epc_len", c_uint8),
                        ("epc", ARRAY5),
                        ("tid_len", c_uint8),
                        ("tid", ARRAY5),
                        ("data_len", c_uint8),
                        ("usr_data", ARRAY5),
                        ("has_timestamp", c_bool),
                        ("tag_time_ms", c_uint),
                        ("has_rssi", c_bool),
                        ("rssi", c_uint),
                        ("has_antenna", c_bool),
                        ("antenna", c_uint)]
        self.lib.get_tag_data.restype = POINTER(TAG_DATA)
        del _pointer_type_cache[TAG_DATA]
        gc.collect()
        return self.lib.get_tag_data(c_uint8(row_no))

    def control_heat(self, lat_tm, inv_tm):
        return self.lib.control_heat(c_longlong(lat_tm),c_longlong(inv_tm))

    def Set_DeviceConfigData(self, dev_cfg):
        """ Set_DeviceConfigData api

        :param function: make device configuration settings
        :return: error if data is not valid
        :rtype: int
        """
        class DeviceConfiguration(Structure):
            _fields_ = [("fCallBackMain", c_bool),
                        ("fLoopOpearation", c_bool),
                        ("uchDeviceOpMode", c_uint8),
                        ("uchMaxTagVicinityCount", c_uint8),
                        ("uchTagRepeateCount", c_uint8),
                        ("uiSensorOffTime", c_uint16),
                        ("uiSensorOnTime", c_uint16)]
        self.lib.Set_DeviceConfigData.argtypes  = [POINTER(DeviceConfiguration)]
        del _pointer_type_cache[DeviceConfiguration]
        gc.collect()
        return self.lib.Set_DeviceConfigData(byref(DeviceConfiguration(dev_cfg)))

    def uhf_set_inventory(self, stg_inv):
        """ uhf_set_inventory api

        :param function: make inventory configuration settings
        :return: error if data is not valid
        :rtype: int
        """

        class SettingInventory(Structure):
            _fields_ = [("fEnableFastID", c_bool),
                        ("fEnableTagFocus", c_bool),
                        ("fEnableTagOperation", c_bool),
                        ("fReqCRC", c_bool),
                        ("fReqPC", c_bool),
                        ("fReqRSSI", c_bool),
                        ("fReqTID", c_bool),
                        ("fReqTimestamp", c_bool),
                        ("fReqAntenna", c_bool),
                        ("uchRfMode", c_uint8),
                        ("uiPopulationTag", c_uint16),
                        ("uisearchMode", c_uint16),
                        ("uiSession", c_uint16),
                        ("uiStopDuration", c_uint16),
                        ("uiStopTagCount", c_uint16)]
        self.lib.uhf_set_inventory.argtypes  = [POINTER(SettingInventory)]
        del _pointer_type_cache[SettingInventory]
        gc.collect()
        return self.lib.uhf_set_inventory(byref(SettingInventory(stg_inv)))

    def start_inventory(self):
        return self.lib.start_inventory()

    def stop_inventory(self):
        return self.lib.stop_inventory()
