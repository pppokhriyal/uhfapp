import sqlite3
import logging

class Apistorage(object):
    def __init__(self):
        try:
            self.con = None
            self.path = "/usr/local/db/api.db"
        except Exception,e:
            logging.error('%s ',e)
            pass

    def put_qdata(self, q_data):
        try:
            con = self.get_curser()
            cur = con.cursor()
            cur.executemany('INSERT INTO apistorage VALUES (?,?,?,?,?,?,?)', q_data)
            con.commit()
            cur.close()
            con.close()
        except Exception, e:
            logging.error('%s ',e)
            pass

    def get_curser(self): #"connect and return curser"
        con = sqlite3.connect(self.path)
        return con

if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m-%d-%Y %I:%M:%S %p',filename='/var/log/apistorage.log',level=logging.DEBUG)
