#!/usr/bin/env python

import sqlite3
import threading
import json
import requests
from time import sleep
import configparser

def clear_db_flag1(delrowid):
    try:
        con = get_conn()
        cur = con.cursor()
        for i in delrowid:
            cur.execute("DELETE FROM customstorage where rowid=?",(i,))
            con.commit()
        cur.close()
        con.close()
    except Exception,e:
        print "Error:Deleting Flag=1 data {0}".format(e)

def push_data(rec_data,idrow):

    """check if data len is zero"""
    if len(rec_data[0]) != 0:
        try:
            con = get_conn()
            cur = con.cursor()
            headers = {'Content-Type': 'application/json'}
            r = requests.post(url,data=json.dumps(rec_data),headers=headers,timeout=url_timeout)

            if r.status_code==201 or r.status_code==200:
                for i in idrow:
                    cur.execute("UPDATE customstorage SET flag=1 WHERE rowid=?",(i,))
                    con.commit()

                cur.close()
                con.close()
                #clear_db_flag1(idrow)
            else:
                print "No response from API"
        except Exception,e:
            print e

def get_data():
    def dict_factory(cursor,row):
        d = {}
        for idx,col in enumerate(cursor.description):
            d[col[0]] = row[idx]
        return d

    while True:

        """custom url data format"""
        parent_dict = {}
        child_dict  = {}
        datalist    = []

        con = get_conn()
        con.row_factory = dict_factory
        cur = con.cursor()

        #check if row count is empty
        cur.execute("SELECT COUNT(*) FROM customstorage")
        result_cur=cur.fetchall()

        if result_cur[0].values()[0] > 0 :
            cur.execute("SELECT rowid,epc_id,timestamp FROM customstorage WHERE flag=0 LIMIT 100")
            row_list = [i['rowid'] for i in cur.fetchall()]
            cur.execute("SELECT rowid,epc_id,timestamp FROM customstorage WHERE flag=0 LIMIT 100")
            data_result = cur.fetchall()

            """create custom data format"""
            for i in data_result:
                child_dict["TAG_ID"] = i["epc_id"]
                child_dict["DETECTED_DATE_TIME"] = i["timestamp"]
                child_dict["LATTITUDE"] = lattitude
                child_dict["LONGITUDE"] = longitude
                parent_dict["tableName"] = tablename
                parent_dict["Imei"] = macid
                parent_dict["Data"] = child_dict


            datalist.append(parent_dict)
            push_data(datalist,row_list)

            cur.close()
            con.close()
        else:
            cur.close()
            con.close()
        sleep(1)

def get_conn():
    conn = sqlite3.connect('/usr/local/db/database.db',check_same_thread=False)
    return conn

def main():
    t = threading.Thread(target=get_data)
    t.start()

if __name__ == '__main__':

    """read api configuration ini"""
    config = configparser.ConfigParser()
    config.read('/etc/uhf/reader.ini')
    url=str(config['Reader']['apiurl'])
    url_timeout = int(config['Reader']['apitimeout'])

    """read custom url attributes"""
    tablename="DUMP_DATATRAP_ANDROID"
    macid=str(config['Reader']['readerid'])
    lattitude="0.0"
    longitude="0.0"

    """start main"""
    main()