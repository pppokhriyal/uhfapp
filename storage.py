import sqlite3
import logging

class Storage(object):
    def __init__(self):
        try:
            self.con = None
            self.path = "/usr/local/db/database.db"
        except Exception,e:
            logging.error('%s ',e)
            pass

    def put_qdata(self, q_data):
        try:
            con = self.get_curser()
            cur = con.cursor()
            cur.executemany('INSERT INTO customstorage VALUES (?,?,?,?,?,?,?)', q_data)
            con.commit()
            #once the data reaches 10000 remove first 1000 data
            count_10000_data = cur.execute("select count(*) from customstorage")
            if count_10000_data.fetchall()[0][0] >= 10000:
                print "Reached 10000 Data"
                cur.execute("delete from customstorage where rowid IN (select rowid from customstorage where flag=1 limit 1000)")
                con.commit()
            cur.close()
            con.close()
        except Exception, e:
            logging.error('%s ',e)
            pass

    def get_curser(self):
        con = sqlite3.connect(self.path)
        return con

if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m-%d-%Y %I:%M:%S %p',filename='/var/log/storage.log',level=logging.DEBUG)
